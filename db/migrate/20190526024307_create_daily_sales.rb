class CreateDailySales < ActiveRecord::Migration[5.1]
  def change
    create_table :daily_sales do |t|
      t.string :product_name
      t.string :customer_name
      t.integer :pri_quantity
      t.integer :sec_quantity
      t.integer :pri_rate
      t.integer :sec_rate
      t.integer :total

      t.timestamps
    end
  end
end
