class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :customer_name
      t.string :product_name
      t.integer :pri_quantity
      t.integer :sec_quantity

      t.timestamps
    end
  end
end
