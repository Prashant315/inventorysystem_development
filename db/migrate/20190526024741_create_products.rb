class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name
      t.integer :pri_quantity
      t.integer :sec_quantity
      t.integer :pri_rate
      t.integer :sec_rate

      t.timestamps
    end
  end
end
