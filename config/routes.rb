Rails.application.routes.draw do
  root 'sales#index'
  devise_for :users
  resources :sales
  resources :products
  resources :orders
  resources :monthly_sales
  resources :daily_sales
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
