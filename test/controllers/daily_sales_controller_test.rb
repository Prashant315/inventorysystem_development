require 'test_helper'

class DailySalesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @daily_sale = daily_sales(:one)
  end

  test "should get index" do
    get daily_sales_url
    assert_response :success
  end

  test "should get new" do
    get new_daily_sale_url
    assert_response :success
  end

  test "should create daily_sale" do
    assert_difference('DailySale.count') do
      post daily_sales_url, params: { daily_sale: { customer_name: @daily_sale.customer_name, pri_quantity: @daily_sale.pri_quantity, pri_rate: @daily_sale.pri_rate, product_name: @daily_sale.product_name, sec_quantity: @daily_sale.sec_quantity, sec_rate: @daily_sale.sec_rate, total: @daily_sale.total } }
    end

    assert_redirected_to daily_sale_url(DailySale.last)
  end

  test "should show daily_sale" do
    get daily_sale_url(@daily_sale)
    assert_response :success
  end

  test "should get edit" do
    get edit_daily_sale_url(@daily_sale)
    assert_response :success
  end

  test "should update daily_sale" do
    patch daily_sale_url(@daily_sale), params: { daily_sale: { customer_name: @daily_sale.customer_name, pri_quantity: @daily_sale.pri_quantity, pri_rate: @daily_sale.pri_rate, product_name: @daily_sale.product_name, sec_quantity: @daily_sale.sec_quantity, sec_rate: @daily_sale.sec_rate, total: @daily_sale.total } }
    assert_redirected_to daily_sale_url(@daily_sale)
  end

  test "should destroy daily_sale" do
    assert_difference('DailySale.count', -1) do
      delete daily_sale_url(@daily_sale)
    end

    assert_redirected_to daily_sales_url
  end
end
