require 'test_helper'

class SalesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sale = sales(:one)
  end

  test "should get index" do
    get sales_url
    assert_response :success
  end

  test "should get new" do
    get new_sale_url
    assert_response :success
  end

  test "should create sale" do
    assert_difference('Sale.count') do
      post sales_url, params: { sale: { customer_name: @sale.customer_name, pri_quantity: @sale.pri_quantity, pri_rate: @sale.pri_rate, product_name: @sale.product_name, sec_quantity: @sale.sec_quantity, sec_rate: @sale.sec_rate, total: @sale.total } }
    end

    assert_redirected_to sale_url(Sale.last)
  end

  test "should show sale" do
    get sale_url(@sale)
    assert_response :success
  end

  test "should get edit" do
    get edit_sale_url(@sale)
    assert_response :success
  end

  test "should update sale" do
    patch sale_url(@sale), params: { sale: { customer_name: @sale.customer_name, pri_quantity: @sale.pri_quantity, pri_rate: @sale.pri_rate, product_name: @sale.product_name, sec_quantity: @sale.sec_quantity, sec_rate: @sale.sec_rate, total: @sale.total } }
    assert_redirected_to sale_url(@sale)
  end

  test "should destroy sale" do
    assert_difference('Sale.count', -1) do
      delete sale_url(@sale)
    end

    assert_redirected_to sales_url
  end
end
