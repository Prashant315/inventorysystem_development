require 'test_helper'

class MonthlySalesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @monthly_sale = monthly_sales(:one)
  end

  test "should get index" do
    get monthly_sales_url
    assert_response :success
  end

  test "should get new" do
    get new_monthly_sale_url
    assert_response :success
  end

  test "should create monthly_sale" do
    assert_difference('MonthlySale.count') do
      post monthly_sales_url, params: { monthly_sale: { customer_name: @monthly_sale.customer_name, pri_quantity: @monthly_sale.pri_quantity, pri_rate: @monthly_sale.pri_rate, product_name: @monthly_sale.product_name, sec_quantity: @monthly_sale.sec_quantity, sec_rate: @monthly_sale.sec_rate, total: @monthly_sale.total } }
    end

    assert_redirected_to monthly_sale_url(MonthlySale.last)
  end

  test "should show monthly_sale" do
    get monthly_sale_url(@monthly_sale)
    assert_response :success
  end

  test "should get edit" do
    get edit_monthly_sale_url(@monthly_sale)
    assert_response :success
  end

  test "should update monthly_sale" do
    patch monthly_sale_url(@monthly_sale), params: { monthly_sale: { customer_name: @monthly_sale.customer_name, pri_quantity: @monthly_sale.pri_quantity, pri_rate: @monthly_sale.pri_rate, product_name: @monthly_sale.product_name, sec_quantity: @monthly_sale.sec_quantity, sec_rate: @monthly_sale.sec_rate, total: @monthly_sale.total } }
    assert_redirected_to monthly_sale_url(@monthly_sale)
  end

  test "should destroy monthly_sale" do
    assert_difference('MonthlySale.count', -1) do
      delete monthly_sale_url(@monthly_sale)
    end

    assert_redirected_to monthly_sales_url
  end
end
