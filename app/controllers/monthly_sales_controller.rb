class MonthlySalesController < ApplicationController
  before_action :set_monthly_sale, only: [:show, :edit, :update, :destroy]

  # GET /monthly_sales
  # GET /monthly_sales.json
  def index
    @monthly_sales = MonthlySale.all
  end

  # GET /monthly_sales/1
  # GET /monthly_sales/1.json
  def show
  end

  # GET /monthly_sales/new
  def new
    @monthly_sale = MonthlySale.new
  end

  # GET /monthly_sales/1/edit
  def edit
  end

  # POST /monthly_sales
  # POST /monthly_sales.json
  def create
    @monthly_sale = MonthlySale.new(monthly_sale_params)

    respond_to do |format|
      if @monthly_sale.save
        format.html { redirect_to @monthly_sale, notice: 'Monthly sale was successfully created.' }
        format.json { render :show, status: :created, location: @monthly_sale }
      else
        format.html { render :new }
        format.json { render json: @monthly_sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /monthly_sales/1
  # PATCH/PUT /monthly_sales/1.json
  def update
    respond_to do |format|
      if @monthly_sale.update(monthly_sale_params)
        format.html { redirect_to @monthly_sale, notice: 'Monthly sale was successfully updated.' }
        format.json { render :show, status: :ok, location: @monthly_sale }
      else
        format.html { render :edit }
        format.json { render json: @monthly_sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /monthly_sales/1
  # DELETE /monthly_sales/1.json
  def destroy
    @monthly_sale.destroy
    respond_to do |format|
      format.html { redirect_to monthly_sales_url, notice: 'Monthly sale was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_monthly_sale
      @monthly_sale = MonthlySale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def monthly_sale_params
      params.require(:monthly_sale).permit(:product_name, :customer_name, :pri_quantity, :sec_quantity, :pri_rate, :sec_rate, :total)
    end
end
