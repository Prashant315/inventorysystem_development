json.extract! product, :id, :product_name, :pri_quantity, :sec_quantity, :pri_rate, :sec_rate, :created_at, :updated_at
json.url product_url(product, format: :json)
