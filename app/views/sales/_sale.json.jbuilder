json.extract! sale, :id, :product_name, :customer_name, :pri_quantity, :sec_quantity, :pri_rate, :sec_rate, :total, :created_at, :updated_at
json.url sale_url(sale, format: :json)
