json.extract! daily_sale, :id, :product_name, :customer_name, :pri_quantity, :sec_quantity, :pri_rate, :sec_rate, :total, :created_at, :updated_at
json.url daily_sale_url(daily_sale, format: :json)
