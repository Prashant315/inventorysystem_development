json.extract! monthly_sale, :id, :product_name, :customer_name, :pri_quantity, :sec_quantity, :pri_rate, :sec_rate, :total, :created_at, :updated_at
json.url monthly_sale_url(monthly_sale, format: :json)
